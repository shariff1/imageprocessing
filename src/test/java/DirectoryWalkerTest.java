import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import scan.DirectoryWalker;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DirectoryWalkerTest {
    DirectoryWalker directoryWalker;
    String pathToDirectoryWalker;
    Path actualPath;

    @Before
    public void setUp() {
        directoryWalker = new DirectoryWalker();
        pathToDirectoryWalker = "src/test/resources/test";

    }

    @Test
    public void directoryWalkerShouldReturnListofFilePaths() {
        try {
            List<Path> actual = directoryWalker.getListOfFiles(pathToDirectoryWalker);
            List<Path> expectedFromMe = new ArrayList<>();

            List<String> resourcePaths = new ArrayList<>();
            resourcePaths.add("src/test/resources/test/0730870173855-ma.jpg");
            resourcePaths.add("src/test/resources/test/0730870173930-ma-detail.jpg");
            resourcePaths.add("src/test/resources/test/0730870173930-ma.jpg");

            for (String path : resourcePaths
                    ) {
                actualPath = Paths.get(path);
                expectedFromMe.add(actualPath);
            }

            assertEquals(expectedFromMe, actual);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void directoryWalkerShouldThorwExceptionFor_getListOfFiles_Null() {
        try {
            directoryWalker.getListOfFiles(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void directoryWalkerShouldThorwExceptionFor_getListOfFiles_Blank() {
        try {
            directoryWalker.getListOfFiles("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        directoryWalker = null;

    }


}
