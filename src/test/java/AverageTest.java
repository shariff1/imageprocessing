import compute.ComputeFiles;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class AverageTest {
    private static final double DELTA = 1e-15;
    ComputeFiles computeFiles;

    @Before
    public void setUp() {
        computeFiles = new ComputeFiles();

    }

    @After
    public void tearDown() {
        computeFiles = null;

    }

    @Test
    public void testHeightAverageReturnsDouble() {

        List<Double> heightDouble = new ArrayList<>();
        heightDouble.add(1d);
        heightDouble.add(10d);
        heightDouble.add(15d);

        Double actual = computeFiles.getHeightAverage(heightDouble);
        Double result = Math.abs(actual);

        assertEquals(8.666666666666666, result, DELTA);
    }

}
