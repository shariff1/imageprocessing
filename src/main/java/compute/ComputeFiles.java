package compute;

import cache.LRUCacheImpl;
import entity.Dimension;
import entity.GifDimension;
import info.PictureInfo;
import print.PrintResult;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


public class ComputeFiles {
    /***
     * gets all the dimensions and compressions of images in the folder
     * @param lruCache
     * @param files
     * @param dimension
     * @param compressionList
     * @param dimensionHeightList
     * @param dimensionWidthList
     * @param pictureInfo
     * @param gifDimension
     */
    public void getDimensionOfImage(LRUCacheImpl lruCache, List<Path> files, Dimension dimension, List<Double> compressionList, List<Double> dimensionHeightList, List<Double> dimensionWidthList, PictureInfo pictureInfo, GifDimension gifDimension) {
        files.forEach(file -> {

            long jpegLength = pictureInfo.getFileSize(file.toFile());
            java.awt.Dimension dimenstio = pictureInfo.getJPEGSize(file.toFile());

            String fileName = file.getFileName().toString();
            String key = removeExtension(fileName);
            Path path = file.toAbsolutePath();

            int[] gifSize = pictureInfo.getGIFSize(file.toFile());
            gifDimension.setGifSize(gifSize);


            if (dimenstio != null) {

                dimension.setHeight(dimenstio.getHeight());
                dimension.setWidth(dimenstio.getWidth());
                dimension.setSize(dimenstio.getSize());
                dimension.setPath(path);
                lruCache.put(key, dimension);


                compressionList.add(jpegLength * 100 / (dimension.getSize().getWidth() * dimension.getHeight() * 3));
                dimensionHeightList.add(dimension.getHeight());
                dimensionWidthList.add(dimension.getWidth());

            }

        });
    }

    /***
     * gets the average of width
     * @param dimensionWidthList
     * @return
     */
    public Double getWidthAverage(List<Double> dimensionWidthList) {
        Double sum = 0d;
        if (!dimensionWidthList.isEmpty()) {
            for (Double compression : dimensionWidthList
                    ) {
                sum += compression;

            }
            sum = sum / dimensionWidthList.size();

        }

        return sum;
    }

    /***
     * get the average of height
     * @param dimensionHeightList
     * @return
     */
    public Double getHeightAverage(List<Double> dimensionHeightList) {
        Double sum = 0d;
        if (!dimensionHeightList.isEmpty()) {
            for (Double compression : dimensionHeightList
                    ) {
                sum += compression;

            }
            sum = sum / dimensionHeightList.size();

        }

        return sum;
    }

    /***
     * provides file name
     * @param fileName
     * @return
     */
    private static String removeExtension(String fileName) {
        String separator = System.getProperty("file.separator");
        String filename;

        // Remove the path upto the filename.
        int lastSeparatorIndex = fileName.lastIndexOf(separator);
        if (lastSeparatorIndex == -1) {
            filename = fileName;
        } else {
            filename = fileName.substring(lastSeparatorIndex + 1);
        }

        // Remove the extension.
        int extensionIndex = filename.lastIndexOf(".");
        if (extensionIndex == -1)
            return filename;

        return filename.substring(0, extensionIndex);
    }

    /***
     * computes the compression average
     * @param compressionList
     * @return
     */
    public Double getCompressionAveage(List<Double> compressionList) {
        Double sum = 0d;
        if (!compressionList.isEmpty()) {
            for (Double compression : compressionList
                    ) {
                sum += compression;

            }
            sum = sum / compressionList.size();
        }

        return sum;
    }

    /***
     * Stores the resulting dimensions and compressions IN-memory
      * @param files
     * @param result
     * @param size
     * @param pathName
     */
    public void executeComputation(List<Path> files, List<Path> result, Long size, String pathName) {
        LRUCacheImpl lruCache = new LRUCacheImpl(4, 0.75f);
        GifDimension gifDimension = new GifDimension();
        Dimension dimension = new Dimension();
        List<Double> compressionList = new ArrayList<>();
        List<Double> dimensionHeightList = new ArrayList<>();
        List<Double> dimensionWidthList = new ArrayList<>();
        PictureInfo pictureInfo = new PictureInfo();

        getDimensionOfImage(lruCache, files, dimension,
                compressionList, dimensionHeightList, dimensionWidthList,
                pictureInfo, gifDimension);
        getDimensionOfImage(lruCache, result, dimension,
                compressionList, dimensionHeightList, dimensionWidthList,
                pictureInfo, gifDimension);
        Double compressionResult = getCompressionAveage(compressionList);
        Double heightResult = getHeightAverage(dimensionHeightList);
        Double WidthResult = getWidthAverage(dimensionWidthList);
        PrintResult printResult = new PrintResult();
        printResult.printDirectory(size, heightResult, WidthResult, compressionResult, pathName);
        for (Path path : files
                ) {
            printResult.printJPEG(dimension.getWidth(), dimension.getHeight(), pictureInfo.getFileSize(path.toFile()), dimension.getPath());
        }
        for (Path gifPath : result) {
            printResult.printGif(gifDimension.getGifSize(), dimension.getPath(), pictureInfo.getFileSize(gifPath.toFile()));
        }
    }
}
