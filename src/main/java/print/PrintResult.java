package print;

import java.nio.file.Path;

public class PrintResult {
    /***
     * Prints the directory structure
     * @param size
     * @param heightResult
     * @param widthResult
     * @param compressionResult
     * @param path
     */
    public void printDirectory(long size, Double heightResult, Double widthResult, Double compressionResult, String path) {

        System.out.println("name: " + path + ", type: Directory");
        System.out.println("  length:      " + size);
        System.out.println("  size:        " + Math.round(widthResult) + " x " + Math.round(heightResult));
        System.out.println("  compression: " + Math.round(compressionResult) + "%");

    }

    /***
     * prints the image structure
     * @param width
     * @param height
     * @param fileSize
     * @param name
     */
    public void printJPEG(double width, double height,long fileSize, Path name) {
        System.out.println("name: " + name + ", type: JPEG-image");
        System.out.println("  length:      " + fileSize);
        System.out.println("  size:        " + Math.round(width) + " x " + Math.round(height));
        System.out.println("  compression: " + Math.round(fileSize * 100 / (width * height * 3)) + "%");
    }

    /***
     * prints the gif structure
     * @param gifSize
     * @param path
     * @param gifLength
     */
    public void printGif(int[] gifSize, Path path, long gifLength) {
        System.out.println ("name: "+path+", type: GIF-image");
        System.out.println ("  length:      "+gifLength);
        System.out.println ("  size:        "+gifSize [0]+" x "+gifSize [1]);
        System.out.println ("  compression: "+(gifLength * 100 / (gifSize [0] * gifSize [1]))+"%");
    }
}
