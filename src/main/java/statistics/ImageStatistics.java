package statistics;

import java.io.File;

public class ImageStatistics {
    public long getFolderSize(File folder) {
        long length = 0;
        File[] files = folder.listFiles();
        if (files != null) {
            int count = files.length;

            for (int i = 0; i < count; i++) {
                if (files[i].isFile()) {
                    length += files[i].length();
                } else {
                    length += getFolderSize(files[i]);
                }
            }
        }
        return length;
    }
}
