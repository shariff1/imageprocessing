import compute.ComputeFiles;
import scan.DirectoryWalker;
import statistics.ImageStatistics;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class Main {



    public static void main(String args[]) throws IOException {
        /***
         * @input PATH to image folder
         */
        imageFiles(args[0]);

    }

    /***
     * Computes different format files
     * @param PATH
     * @throws IOException
     */
    private static void imageFiles(String PATH) throws IOException {

        DirectoryWalker walker = new DirectoryWalker();
        List<Path> files = walker.getListOfFiles(PATH);

        List<Path> result = files.stream()                // convert list to stream
                .filter(file -> file.toString().endsWith("gif"))     // filter out gif
                .collect(Collectors.toList());


        int totalFileSize = files.size();
        System.out.println("Total File size: " + totalFileSize);


        File folder = new File(PATH);
        ImageStatistics imageStatistics = new ImageStatistics();
        long size = imageStatistics.getFolderSize(folder);

        ComputeFiles compute = new ComputeFiles();
        compute.executeComputation(files, result, size, PATH);
    }


}
