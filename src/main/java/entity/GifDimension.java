package entity;

public class GifDimension {
    long gifLength;
    int[]  gifSize;

    public int[] getGifSize() {
        return gifSize;
    }

    public void setGifSize(int[] gifSize) {
        this.gifSize = gifSize;
    }


    public long getGifLength() {
        return gifLength;
    }

    public void setGifLength(long gifLength) {
        this.gifLength = gifLength;
    }


}
