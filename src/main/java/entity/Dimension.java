package entity;

import java.nio.file.Path;

public class Dimension {

    double height;
    double width;
    java.awt.Dimension size;
    Path path;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }





    public java.awt.Dimension getSize() {
        return size;
    }

    public void setSize(java.awt.Dimension size) {
        this.size = size;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }


}
