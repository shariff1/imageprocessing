package scan;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


public class DirectoryWalker {
    /***
     * walks through the directory structure
     * @param path
     * @return
     * @throws IOException
     */
    public List<Path> getListOfFiles(String path) throws IOException {

        if (path == null || path.equals("")) throw new IllegalArgumentException("No folder path provided as an input");

        List<Path> filesToParse = new ArrayList<Path>();

        try (Stream<Path> paths = Files.walk(Paths.get(path))) {
            paths.forEach(filePath ->
                    {
                        try {
                            if (Files.probeContentType(filePath.getFileName()) != null) {
                                if (filePath.getFileName().toString().endsWith("jpg") || filePath.getFileName().toString().endsWith("png") ||
                                        filePath.getFileName().toString().endsWith("gif")) {
                                    filesToParse.add(filePath);
                                }

                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
        } catch (NoSuchFileException e) {
            throw new NoSuchFileException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getCause());
        }

        return filesToParse;
    }

}
